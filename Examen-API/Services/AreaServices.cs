﻿using Examen_API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Examen_API.Services
{
    public interface IAreaServices
    {
        public IEnumerable<dynamic> getAreas();
        public dynamic getAreaById(int id);
        public bool createArea(Area obj);
        public bool updateArea(Area obj);
        public bool deleteAreaById(int id);

    }
    public class AreaServices : IAreaServices
    {
        //DI for Context
        private ExamenContext context;
        
        public AreaServices(ExamenContext _context) { this.context = _context; }

        public IEnumerable<dynamic> getAreas()
        {
            try {
                if (context.Area.Count() > 0)
                {
                    return context.Area.Select(a => new {
                        idArea = a.IdArea,
                        Nombre = a.Nombre,
                        Descripcion = a.Descripcion
                    });
                } else
                {
                    return null;
                }
               
            }
            catch
            {
                return null;
            }
        }

        public dynamic getAreaById(int id)
        {
            try
            {
                if (context.Area.Where(a => a.IdArea == id).Count() > 0)
                {
                    return context.Area.Where(a => a.IdArea == id).Select(a => new { 
                        idArea = a.IdArea,
                        Nombre = a.Nombre,
                        Descripcion = a.Descripcion
                    });
                } else
                {
                    return null;
                }
             
            }
            catch
            {
                return null;
            }
        }
        public bool createArea(Area obj)
        {
            try
            {
                context.Area.Add(obj);
                context.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool updateArea(Area obj)
        {
            try
            {
                context.Area.Update(obj);
                context.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool deleteAreaById(int id)
        {
            try
            {
                context.Area.Remove(context.Area.Find(id));
                context.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

    }
}
