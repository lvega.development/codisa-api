﻿using Examen_API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Examen_API.Services
{
    public interface IEmpleadoServices
    {
        public IEnumerable<dynamic> getEmpleados();
        public IEnumerable<Empleado> getEmpleadoById(int id);

        public bool createEmpleado(Empleado obj);

        public bool updateEmpleado(Empleado obj);
        public bool deleteEmpleadoById(int id);
        public dynamic getFicha(int id);
        public dynamic getTree();
    }
    public class EmpleadoServices : IEmpleadoServices
    {
        //DI for Context
        private ExamenContext context;

        public EmpleadoServices(ExamenContext _context) { this.context = _context; }

        public IEnumerable<dynamic> getEmpleados()
        {
            try
            {
                DateTime zeroTime = new DateTime(1, 1, 1);
                DateTime now = DateTime.Now;
                if (context.Empleado.Count() > 0)
                {
                    return context.Empleado.Select(e => new {
                        idEmpleado = e.IdEmpleado,
                        cedula = e.Cedula,
                        nombreCompleto = e.NombreCompleto,
                        correo = e.Correo,
                        fechaIngreso = e.FechaIngreso,
                        fechaNacimiento = e.FechaNacimiento,
                        idJefe = e.IdJefe,
                        idArea = e.IdArea,
                        foto = e.Foto,
                        age = (now.Year - Convert.ToDateTime(e.FechaNacimiento.ToString()).Year - 1) + (((now.Month > Convert.ToDateTime(e.FechaNacimiento.ToString()).Month) || ((now.Month == Convert.ToDateTime(e.FechaNacimiento.ToString()).Month) && (now.Day >= Convert.ToDateTime(e.FechaNacimiento.ToString()).Day))) ? 1 : 0),
                        workYears = (now.Year - Convert.ToDateTime(e.FechaIngreso.ToString()).Year - 1) + (((now.Month > Convert.ToDateTime(e.FechaIngreso.ToString()).Month) || ((now.Month == Convert.ToDateTime(e.FechaIngreso.ToString()).Month) && (now.Day >= Convert.ToDateTime(e.FechaIngreso.ToString()).Day))) ? 1 : 0)
                    });
                }
                else
                {
                    return null;
                }

            }
            catch
            {
                return null;
            }
        }

        public dynamic getFicha(int id)
        {
            try
            {
                DateTime zeroTime = new DateTime(1, 1, 1);
                DateTime now = DateTime.Now;
                if (context.Empleado.Count() > 0)
                {
                    return context.Empleado.Where(a => a.IdEmpleado == id).Select(e => new {
                        idEmpleado = e.IdEmpleado,
                        cedula = e.Cedula,
                        nombreCompleto = e.NombreCompleto,
                        correo = e.Correo,
                        fechaIngreso = e.FechaIngreso,
                        fechaNacimiento = e.FechaNacimiento,
                        idJefe = e.IdJefe,
                        idArea = e.IdArea,
                        foto = e.Foto,
                        age = (now.Year - Convert.ToDateTime(e.FechaNacimiento.ToString()).Year - 1) + (((now.Month > Convert.ToDateTime(e.FechaNacimiento.ToString()).Month) || ((now.Month == Convert.ToDateTime(e.FechaNacimiento.ToString()).Month) && (now.Day >= Convert.ToDateTime(e.FechaNacimiento.ToString()).Day))) ? 1 : 0),
                        workYears = (now.Year - Convert.ToDateTime(e.FechaIngreso.ToString()).Year - 1) + (((now.Month > Convert.ToDateTime(e.FechaIngreso.ToString()).Month) || ((now.Month == Convert.ToDateTime(e.FechaIngreso.ToString()).Month) && (now.Day >= Convert.ToDateTime(e.FechaIngreso.ToString()).Day))) ? 1 : 0)
                    });
                }
                else
                {
                    return null;
                }

            }
            catch
            {
                return null;
            }
        }

        public dynamic getTree()
        {
            try
            {
    
                if (context.Empleado.Count() > 0)
                {
                    return context.Empleado.Select(e => e).ToList();
                }
                else
                {
                    return null;
                }

            }
            catch
            {
                return null;
            }
        }

        public IEnumerable<Empleado> getEmpleadoById(int id)
        {
            try
            {
                if (context.Empleado.Where(a => a.IdEmpleado == id).Count() > 0)
                {
                    return context.Empleado.Where(a => a.IdEmpleado == id).Select(e => e).ToList();
                }
                else
                {
                    return null;
                }

            }
            catch
            {
                return null;
            }
        }
        public bool createEmpleado(Empleado obj)
        {
            try
            { 
                context.Empleado.Add(obj);
                context.SaveChanges();
                return true;
            }
            catch(Exception e)
            {
                var p = e;
                return false;
            }
        }

        public bool updateEmpleado(Empleado obj)
        {
            try
            {
                context.Empleado.Update(obj);
                context.SaveChanges();
                return true;
            }
            catch(Exception e)
            {
                var p = e;
                return false;
            }
        }
        public bool deleteEmpleadoById(int id)
        {
            try
            {
                context.Empleado.Remove(context.Empleado.Find(id));
                context.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
