﻿using Examen_API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Examen_API.Services
{
    public interface IHabilidadesServices
    {
        public IEnumerable<dynamic> getHabilidades();
        public dynamic getHabilidadById(int id);
        public bool createHabilidad(EmpleadoHabilidad obj);
        public bool updateHabilidad(EmpleadoHabilidad obj);
        public bool deleteHabilidadById(int id);
        public dynamic getHabilidadesByEmpleadoId(int id);
    }
    public class HabilidadesServices : IHabilidadesServices
    {
        //DI for Context
        private ExamenContext context;

        public HabilidadesServices(ExamenContext _context) { this.context = _context; }

        public IEnumerable<dynamic> getHabilidades()
        {
            try
            {
                if (context.EmpleadoHabilidad.Count() > 0)
                {
                    return context.EmpleadoHabilidad.Select(h => new {
                        idHabilidad = h.IdHabilidad,
                        NombreHabilidad = h.NombreHabilidad,
                        IdEmpleado = h.IdEmpleado
                    });
                }
                else
                {
                    return null;
                }

            }
            catch
            {
                return null;
            }
        }

        public dynamic getHabilidadById(int id)
        {
            try
            {
                if (context.EmpleadoHabilidad.Where(h => h.IdHabilidad == id).Count() > 0)
                {
                    return context.EmpleadoHabilidad.Where(h => h.IdHabilidad == id).Select(h => new {
                        idHabilidad = h.IdHabilidad,
                        NombreHabilidad = h.NombreHabilidad,
                        IdEmpleado = h.IdEmpleado
                    });
                }
                else
                {
                    return null;
                }

            }
            catch
            {
                return null;
            }
        }

        public dynamic getHabilidadesByEmpleadoId(int id)
        {
            try
            {
                if (context.EmpleadoHabilidad.Where(h => h.IdEmpleado == id).Count() > 0)
                {
                    return context.EmpleadoHabilidad.Where(h => h.IdEmpleado == id).Select(h => new {
                        idHabilidad = h.IdHabilidad,
                        NombreHabilidad = h.NombreHabilidad,
                        IdEmpleado = h.IdEmpleado
                    });
                }
                else
                {
                    return null;
                }

            }
            catch
            {
                return null;
            }
        }
        public bool createHabilidad(EmpleadoHabilidad obj)
        {
            try
            {
                context.EmpleadoHabilidad.Add(obj);
                context.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool updateHabilidad(EmpleadoHabilidad obj)
        {
            try
            {
                context.EmpleadoHabilidad.Update(obj);
                context.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool deleteHabilidadById(int id)
        {
            try
            {
                context.EmpleadoHabilidad.Remove(context.EmpleadoHabilidad.Find(id));
                context.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
