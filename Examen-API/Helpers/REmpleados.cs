﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Examen_API.Helpers
{
    public class REmpleados
    {
        public string  Cedula { get; set; }
        public string Correo { get; set; }
        public string FechaIngreso { get; set; }
        public string FechaNacimiento { get; set; }
        public int IdArea { get; set; }
        public string  NombreCompleto { get; set; }
        public int?  IdJefe { get; set; }
    }
}
