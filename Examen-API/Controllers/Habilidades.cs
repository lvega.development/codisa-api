﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Examen_API.Models;
using Examen_API.Services;
using Microsoft.AspNetCore.Mvc;

namespace Examen_API.Controllers
{
    [Route("api/habilidades")]
    public class Habilidades : Controller
    {
        private IHabilidadesServices habilidadesServices;

        public Habilidades(IHabilidadesServices _habilidadesServices)
        {
            this.habilidadesServices = _habilidadesServices;
        }

        [HttpGet]
        public dynamic get()
        {
            var response = habilidadesServices.getHabilidades();
            if (response != null)
            {
                return Ok(new { data = response });
            }
            else
            {
                return Ok(new { data = "" });
            }
        }

        [HttpGet]
        [Route("{id}")]
        public dynamic get(int id)
        {
            var response = habilidadesServices.getHabilidadesByEmpleadoId(id);
            if (response != null)
            {
                return Ok(new { data = response });
            }
            else
            {
                return Ok(new { data = "" });
            }
        }

        [HttpPost]
        public dynamic create([FromBody] EmpleadoHabilidad obj)
        {
            try
            {
                if (obj.NombreHabilidad.Trim() == "")
                {
                    return BadRequest(new { error = "Nombre Habilidad must not be null or empty" });
                }
                else
                {
                    var response = habilidadesServices.createHabilidad(obj);
                    if (response != false)
                    {
                        return Ok(new { data = "Habilidad creada con exito" });
                    }
                    else
                    {
                        return BadRequest(new { data = "Error al crear habilidad" });
                    }
                }
            }
            catch
            {
                return BadRequest(new { data = "Error al crear habilidad" });
            }

        }

        [HttpPut]
        [Route("{id}")]
        public dynamic update(int id, [FromBody] EmpleadoHabilidad obj)
        {
            try
            {
                if (habilidadesServices.getHabilidadById(id) != null)
                {
                    var response = habilidadesServices.updateHabilidad(obj);
                    if (response != false)
                    {
                        return Ok(new { data = "Habilidad actualizada con exito" });
                    }
                    else
                    {
                        return BadRequest(new { data = "Error al actualizar habilidad" });
                    }
                }
                else
                {
                    return Ok(new { data = "Area not found" });
                }
            }
            catch
            {
                return BadRequest(new { data = "Error al actualizar el area" });
            }

        }

        [HttpDelete]
        [Route("{id}")]
        public dynamic delete(int id)
        {
            if (habilidadesServices.getHabilidadById(id) != null)
            {
                var response = habilidadesServices.deleteHabilidadById(id);
                if (response != false)
                {
                    return Ok(new { data = "Habilidad eliminada con exito" });
                }
                else
                {
                    return BadRequest(new { data = "Error al eliminar habilidad" });
                }
            }
            else
            {
                return BadRequest(new { data = "Habilidad not found" });
            }
        }
    }
}
