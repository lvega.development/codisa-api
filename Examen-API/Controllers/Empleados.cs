﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Examen_API.Helpers;
using Examen_API.Models;
using Examen_API.Services;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Routing;

namespace Examen_API.Controllers
{
    [Route("api/empleados")]

    public class Empleados : Controller
    {
        private IEmpleadoServices empleadoServices;
        private IAreaServices areaServices;
        private readonly IHostingEnvironment _environment;

        public Empleados(IEmpleadoServices _empleadoServices, IAreaServices _areaServices, IHostingEnvironment _hostingEnvironment)
        {
            this.empleadoServices = _empleadoServices;
            this._environment = _hostingEnvironment;
            this.areaServices = _areaServices;
        }

        [HttpGet]
        public dynamic getEmpleados()
        {
            var empleados = empleadoServices.getEmpleados();
            if (empleados != null)
            {
                return Ok(new { data = empleados });
            }
            else
            {
                return Ok(new { data = "" });
            }
        }

        [HttpGet]
        [Route("{id}")]
        public dynamic getEmpleados(int id)
        {
            var empleados = empleadoServices.getEmpleadoById(id);
            if (empleados != null)
            {
                return Ok(new { data = empleados });
            }
            else
            {
                return Ok(new { data = "" });
            }
        }

        [HttpGet]
        [Route("ficha/{id}")]
        public dynamic getFicha(int id)
        {
            var empleados = empleadoServices.getFicha(id);
            if (empleados != null)
            {
                return Ok(new { data = empleados });
            }
            else
            {
                return Ok(new { data = "" });
            }
        }

        [HttpGet]
        [Route("tree")]
        public dynamic getTree()
        {
            var tree = empleadoServices.getTree();
            if (tree != null)
            {
                return Ok(new { data = tree });
            }
            else
            {
                return Ok(new { data = "" });
            }
        }

        [HttpPost]
        public dynamic createEmpleado([FromBody]REmpleados obj)
        {
            var p = Convert.ToDateTime("10/05/1995");
            try
            {
                if (obj.IdArea.ToString().Trim() != null)
                {
                    if (areaServices.getAreaById(obj.IdArea) != null)
                    {
                        var empleado = empleadoServices.createEmpleado(new Empleado
                        {
                            NombreCompleto = obj.NombreCompleto,
                            Cedula = obj.Cedula,
                            Correo = obj.Correo,
                            FechaIngreso = Convert.ToDateTime(obj.FechaIngreso),
                            FechaNacimiento = obj.FechaNacimiento.Trim() != ""? Convert.ToDateTime(obj.FechaNacimiento) : DateTime.Now,
                            IdArea = obj.IdArea,
                            IdJefe = obj.IdJefe > 0 ? obj.IdJefe : null
                        });
                        if (empleado != false)
                        {
                            return Ok(new { data = "Empleado creado con exito" });
                        }
                        else
                        {
                            return BadRequest(new { data = "Error al crear el empleado" });
                        }
                    } else
                    {
                        return BadRequest(new { data = "Area not found" });
                    }

                }
                else
                {
                    return BadRequest(new { error = "Area must not be null or empty" });
                }
            }
            catch
            {
                return BadRequest(new { data = "Error al crear el empleado" });
            }

        }

        [HttpPut]
        [Route("{id}")]
        public dynamic updateEmpleado(int id, [FromBody] REmpleados obj)
        {
            try
            {
                var oEmpleado = empleadoServices.getEmpleadoById(id);
                if (oEmpleado != null)
                {
                    if (areaServices.getAreaById(oEmpleado.First().IdArea = obj.IdArea) != null)
                    {
                        oEmpleado.First().IdEmpleado = id;
                        oEmpleado.First().NombreCompleto = obj.NombreCompleto;
                        oEmpleado.First().Cedula = obj.Cedula;
                        oEmpleado.First().Correo = obj.Correo;
                        oEmpleado.First().FechaIngreso = Convert.ToDateTime(obj.FechaIngreso);
                        oEmpleado.First().FechaNacimiento = obj.FechaNacimiento.Trim() != "" ? Convert.ToDateTime(obj.FechaNacimiento) : DateTime.Now;
                        oEmpleado.First().IdArea = obj.IdArea;
                        oEmpleado.First().IdJefe = obj.IdJefe > 0 ? obj.IdJefe : (int?)null;
                        var empleado = empleadoServices.updateEmpleado(oEmpleado.First());
                        if (empleado != false)
                        {
                            return Ok(new { data = "Empleado actualizado con exito" });
                        }
                        else
                        {
                            return BadRequest(new { data = "Error al actualizar el empleado" });
                        }
                    } else
                    {
                        return Ok(new { data = "Area not found" });
                    }
                }
                else
                {
                    return Ok(new { data = "Empleado not found" });
                }
            }
            catch
            {
                return BadRequest(new { data = "Error al actualizar el empleado" });
            }

        }

        [HttpDelete]
        [Route("{id}")]
        public dynamic deleteEmpleado(int id)
        {
            if (empleadoServices.getEmpleadoById(id) != null)
            {
                var empleado = empleadoServices.deleteEmpleadoById(id);
                if (empleado != false)
                {
                    return Ok(new { data = "Empleado eliminado con exito" });
                }
                else
                {
                    return BadRequest(new { data = "Error al eliminar el empleado" });
                }
            }
            else
            {
                return BadRequest(new { data = "Empleado not found" });
            }
        }

        [HttpPost]
        [Route("upload-picture/{idEmpleado}")]
        public dynamic uploadPicture(int idEmpleado, [FromForm(Name = "file")] IFormFile file)
        {
            if (file.Length > 0)
            {
                byte[] imgByte = ReadToEnd(file.OpenReadStream());
                var oEmpleado = empleadoServices.getEmpleadoById(idEmpleado);
                if (oEmpleado != null)
                {
                    oEmpleado.First().Foto = imgByte;
                    var uEmpleado = empleadoServices.updateEmpleado(oEmpleado.First());
                    if (uEmpleado != false)
                    {
                        return Ok(new { data = "Imagen actualizada" });

                    } else
                    {
                        return BadRequest(new { data = "Ha ocurrido un error al subir la imagen" });

                    }
                }
                else
                {
                    return BadRequest(new { data = "Empleado not found" });

                }
                
            } else
            {
                return BadRequest(new { data = "File not found" });

            }
        }

        //Generate random string for image name
        private string randomString()
        {
            int length = 7;
            StringBuilder str_build = new StringBuilder();
            Random random = new Random();
            char letter;
            for (int i = 0; i < length; i++)
            {
                double flt = random.NextDouble();
                int shift = Convert.ToInt32(Math.Floor(25 * flt));
                letter = Convert.ToChar(shift + 65);
                str_build.Append(letter);
            }
            return str_build.ToString();
        }

        public static byte[] ReadToEnd(System.IO.Stream stream)
        {
            long originalPosition = 0;

            if (stream.CanSeek)
            {
                originalPosition = stream.Position;
                stream.Position = 0;
            }

            try
            {
                byte[] readBuffer = new byte[4096];

                int totalBytesRead = 0;
                int bytesRead;

                while ((bytesRead = stream.Read(readBuffer, totalBytesRead, readBuffer.Length - totalBytesRead)) > 0)
                {
                    totalBytesRead += bytesRead;

                    if (totalBytesRead == readBuffer.Length)
                    {
                        int nextByte = stream.ReadByte();
                        if (nextByte != -1)
                        {
                            byte[] temp = new byte[readBuffer.Length * 2];
                            Buffer.BlockCopy(readBuffer, 0, temp, 0, readBuffer.Length);
                            Buffer.SetByte(temp, totalBytesRead, (byte)nextByte);
                            readBuffer = temp;
                            totalBytesRead++;
                        }
                    }
                }

                byte[] buffer = readBuffer;
                if (readBuffer.Length != totalBytesRead)
                {
                    buffer = new byte[totalBytesRead];
                    Buffer.BlockCopy(readBuffer, 0, buffer, 0, totalBytesRead);
                }
                return buffer;
            }
            finally
            {
                if (stream.CanSeek)
                {
                    stream.Position = originalPosition;
                }
            }
        }
    }
}
