﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Examen_API.Models;
using Examen_API.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Routing;

namespace Examen_API.Controllers
{
    [Route("api/areas")]
    public class Areas : Controller
    {
        private IAreaServices areaServices;

        public Areas(IAreaServices _areaServices)
        {
            this.areaServices = _areaServices;
        }

        [HttpGet]
        public dynamic getAreas()
        {
            var areas = areaServices.getAreas();
            if (areas != null)
            {
                return Ok(new { data = areas });
            }
            else
            {
                return Ok(new { data = "" });
            }
        }

        [HttpGet]
        [Route("{id}")]
        public dynamic getAreas(int id)
        {
            var areas = areaServices.getAreaById(id);
            if (areas != null)
            {
                return Ok(new { data = areas });
            }
            else
            {
                return Ok(new { data = "" });
            }
        }

        [HttpPost]
        public dynamic createArea([FromBody] Area obj)
        {
            try
            {
                if (obj.Nombre.Trim() == "")
                {
                    return BadRequest(new { error = "name must not be null or empty" });
                }
                else
                {
                    var area = areaServices.createArea(obj);
                    if (area != false)
                    {
                        return Ok(new { data = "Area creada con exito" });
                    }
                    else
                    {
                        return BadRequest(new { data = "Error al crear el area" });
                    }
                }
            }
            catch
            {
                return BadRequest(new { data = "Error al crear el area" });
            }

        }

        [HttpPut]
        [Route("{id}")]
        public dynamic updateArea(int id, [FromBody]Area obj)
        {
            try
            {
                if (areaServices.getAreaById(id) != null)
                {
                    var area = areaServices.updateArea(obj);
                    if (area != false)
                    {
                        return Ok(new { data = "Area actualizada con exito" });
                    }
                    else
                    {
                        return BadRequest(new { data = "Error al actualizar el area" });
                    }
                }
                else
                {
                    return Ok(new { data = "Area not found" });
                }
            } catch
            {
                return BadRequest(new { data = "Error al actualizar el area" });
            }

        }

        [HttpDelete]
        [Route("{id}")]
        public dynamic deleteArea(int id)
        {
            if (areaServices.getAreaById(id) != null)
            {
                var area = areaServices.deleteAreaById(id);
                if (area != false)
                {
                    return Ok(new { data = "Area eliminada con exito" });
                }
                else
                {
                    return BadRequest(new { data = "Error al eliminar el area" });
                }
            }
            else
            {
                return BadRequest(new { data = "Area not found" });
            }
        }
    }
}
